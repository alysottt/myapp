package com.example.alyso.myapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_delete.*

class Delete : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete)

        backButton.setOnClickListener {
            finish()
        }

        delallButton.setOnClickListener {
            DBHelper(this).deleteAll()
            finish()
        }
    }
}
