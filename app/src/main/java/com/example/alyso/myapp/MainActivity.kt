package com.example.alyso.myapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val lstnm = findViewById<EditText>(R.id.lastnameEnter)
        val nm = findViewById<EditText>(R.id.nameEnter)
        val phn = findViewById<EditText>(R.id.phoneEnter)

        val context = this
        var db = DBHelper(context)

        addButton.setOnClickListener{
            val lname = lstnm.text.toString()
            val name = nm.text.toString()
            val phone = phn.text.toString()
            val i = Intent(this, ConfirmInsert::class.java)
            i.putExtra("lstnmtag", lname)
            i.putExtra("nmtag", name)
            i.putExtra("phntag", phone)
            startActivityForResult(i, 0)
        }

        viewButton.setOnClickListener{
            var data = db.readData()
            tvResult.text = ""
            for (i in 0..(data.size - 1)){
                tvResult.append(data.get(i).id.toString() + " " + data.get(i).name + " " + data.get(i).lastname + " " + data.get(i).phone + "\n")
            }
        }

        dltButton.setOnClickListener {
            startActivityForResult(Intent(this, Delete::class.java), 0)
        }
    }
}
