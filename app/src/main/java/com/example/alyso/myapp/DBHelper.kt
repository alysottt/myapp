package com.example.alyso.myapp

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

val DATABASE_NAME = "MyDB"
val TABLE_NAME = "Clients"
val COL_ID = "id"
val COL_LASTNAME = "lastname"
val COL_NAME = "name"
val COL_PHONE = "phone"

class DBHelper(var context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        val createTable = "CREATE TABLE " + TABLE_NAME + " (" +
                COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL_LASTNAME + " VARCHAR(20), " +
                COL_NAME + " VARCHAR(20), " +
                COL_PHONE + " VARCHAR(15))"
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(client: Client) {
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_LASTNAME, client.lastname)
        cv.put(COL_NAME, client.name)
        cv.put(COL_PHONE, client.phone)
        val result = db.insert(TABLE_NAME, null, cv)
        if (result == (-1).toLong())
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
    }

    fun readData(): MutableList<Client> {
        var list: MutableList<Client> = ArrayList()

        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                val client = Client()
                client.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
                client.name = result.getString(result.getColumnIndex(COL_NAME))
                client.lastname = result.getString(result.getColumnIndex(COL_LASTNAME))
                client.phone = result.getString(result.getColumnIndex(COL_PHONE))
                list.add(client)
            } while (result.moveToNext())
        }
        result.close()
        db.close()
        return list
    }

    fun deleteData(i: Int) {
        val db = this.writableDatabase
        //db.delete(TABLE_NAME, COL_ID + "=?", arrayOf(i.toString()))
        val deleteRow = "DELETE FROM " + TABLE_NAME + " WHERE " +
                "id= " + i
        db?.execSQL(deleteRow)
        db.close()
    }

    fun deleteAll() {
        val db = this.writableDatabase
        db.delete(TABLE_NAME, null, null)
        db.close()
    }

    fun updateData(client: Client, i: Int) {
        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME + " where " + COL_ID + " = " + client.id
        val result = db.rawQuery(query, null)
        if (result.moveToFirst()) {
            do {
                var cv = ContentValues()
                cv.put(COL_LASTNAME, result.getString(result.getColumnIndex(COL_LASTNAME)))
                db.update(TABLE_NAME, cv, COL_ID + "=?" + COL_NAME + "=?", arrayOf(
                    result.getString(result.getColumnIndex(COL_ID)), result.getString(result.getColumnIndex(COL_NAME))
                )
                )
            } while (result.moveToNext())
        }
        result.close()
        db.close()
    }


}