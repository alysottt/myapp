package com.example.alyso.myapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.confirm_insert.*

class ConfirmInsert : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.confirm_insert)

        val lstnmView = findViewById<TextView>(R.id.lastnameView)
        val nmView = findViewById<TextView>(R.id.nameView)
        val phnView = findViewById<TextView>(R.id.phoneView)

        val context = this

        lstnmView.text = intent.getStringExtra("lstnmtag")
        nmView.text = intent.getStringExtra("nmtag")
        phnView.text = intent.getStringExtra("phntag")

        backbtn.setOnClickListener {
            finish()
        }

        confirmbtn.setOnClickListener{
            if (lstnmView.text.toString().isNotEmpty() &&
                nmView.text.toString().isNotEmpty() &&
                phnView.text.toString().isNotEmpty()) {
                var client = Client(lstnmView.text.toString(), nmView.text.toString(), phnView.text.toString())
                var db = DBHelper(context)
                db.insertData(client)
            } else {
                Toast.makeText(context, "Please fill all data`s", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
