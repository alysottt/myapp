package com.example.alyso.myapp

class Client {

    var id : Int = 0;
    var lastname : String = ""
    var name : String = ""
    var phone : String = ""

    constructor(lastname:String, name:String, phone:String){
        this.lastname = lastname
        this.name = name
        this.phone = phone
    }

    constructor(){
    }
}